import glob
import subprocess

for doc in glob.iglob('./wordFiles/*.doc'):
    subprocess.call(['soffice', '--headless', '--convert-to', 'docx', doc, '--outdir','wordFiles'])
