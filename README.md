# DOC to DOCX Converter
Convert DOC to DOCX

## Requirements
- Python >= 3.8
- glob
- subprocess

## Utilisation
1. Prepare a 'wordFiles' folder under the root and put .doc files in it.
2. Run `python convert.py`
